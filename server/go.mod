module kamibako.ltd/code-paste

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.7
	github.com/google/uuid v1.1.2
	github.com/spf13/viper v1.10.1
	gorm.io/driver/mysql v1.2.3
	gorm.io/gorm v1.22.5
)
