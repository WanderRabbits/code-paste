package main

import (
	"github.com/spf13/viper"
	"kamibako.ltd/code-paste/internal/initialize"
	"kamibako.ltd/code-paste/internal/router"
)

func init() {
	initialize.InitConfig()
	initialize.InitDB()
}


func main () {
	r := router.Routers()
	port := viper.GetString("server.port")
	err := r.Run(":" + port)
	if err != nil {
		panic(err)
	}
}
