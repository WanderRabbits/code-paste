package response

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type JsonRes struct {
	Code     int         `json:"code"`     // 错误码((0:成功, 1:失败, >1:错误码))
	Message  string      `json:"message"`  // 提示信息
	Data     interface{} `json:"data"`     // 返回数据(业务接口定义具体数据结构)
}

func Json(ctx *gin.Context, code int, message string, data ...interface{}) {
	responseData := interface{}(nil)
	if len(data) > 0 {
		responseData = data[0]
	}
	ctx.JSON(http.StatusOK, JsonRes{
		Code:    code,
		Message: message,
		Data:    responseData,
	})
}
