package initialize

import (
	"github.com/spf13/viper"
	"os"
)

func InitConfig() {
	workDir, _ := os.Getwd()
	viper.SetConfigName("config")
	viper.SetConfigType("toml")
	viper.AddConfigPath(workDir + "/manifest/config")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}
