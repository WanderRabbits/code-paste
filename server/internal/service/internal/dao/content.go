package dao

import (
	"fmt"
	"github.com/google/uuid"
	"gorm.io/gorm"
	"kamibako.ltd/code-paste/internal/initialize"
	"kamibako.ltd/code-paste/internal/model"
)


var ContentDAO = daoContent{}
type daoContent struct {
}



func DB() *gorm.DB{
	return initialize.DB
}

func (dao *daoContent) GetContentByPath(path string) (content model.Content ,err error) {
	err = DB().Where("path = ?", path).Find(&content).Error
	return
}

func (dao *daoContent) Save(cType, content string) (path string, err error) {
	newUUID, _ := uuid.NewUUID()
	path = string([]byte(fmt.Sprintf("%v", newUUID))[0:8])
	err =  DB().Create(&model.Content{
		Type: cType,
		Content: content,
		Path: path,
	}).Error
	return
}