package service

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"kamibako.ltd/code-paste/internal/service/internal/dao"
	"kamibako.ltd/code-paste/internal/service/internal/do"
	"kamibako.ltd/code-paste/utility/response"
)

type sContent struct {
}

var Content = sContent{}


func (s *sContent) GetContentByPath(c *gin.Context, path string) {
	content, err := dao.ContentDAO.GetContentByPath(path)
	if err != nil {
		response.Json(c,422, "页面不存在")
		return
	}
	response.Json(c,200, "OK", do.Content{
		Content: content.Content,
		Type: content.Type,
	})
}

type ContentDto struct {
	Type string `json:"type" form:"type"`
	Content string `json:"content" form:"content"`
}

func (s *sContent) SaveContent(c *gin.Context, content ContentDto) {
	path, err := dao.ContentDAO.Save(content.Type, content.Content)
	if err != nil {
		response.Json(c, 422, "代码保存失败", fmt.Sprintf("%v", err))
		return
	}
	response.Json(c, 200, "OK", gin.H{
		"path": path,
	})
}