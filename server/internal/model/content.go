package model

import "gorm.io/gorm"

type Content struct {
	gorm.Model
	Content string `json:"content"`
	Type string `json:"type"`
	Path string `json:"path"`
}

func (c Content) TableName() string {
	return "code_paste_content"
}