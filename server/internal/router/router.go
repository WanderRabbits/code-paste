package router

import (
	"github.com/gin-gonic/gin"
	"kamibako.ltd/code-paste/internal/controller"
	"kamibako.ltd/code-paste/internal/middleware"
	"kamibako.ltd/code-paste/utility/response"
)

func Routers() (r *gin.Engine) {
	r = gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(middleware.CorsHandler())
	r.GET("/health", func(ctx *gin.Context) {
		response.Json(ctx, 200, "OK")
	})
	apiv1 := r.Group("/api")
	{
		apiv1.GET("/getContent", controller.Content.GetContentById)
		apiv1.POST("/generateContent", controller.Content.SaveContent)
	}
	return
}