package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"kamibako.ltd/code-paste/internal/service"
	"kamibako.ltd/code-paste/utility/response"
)

var Content = cContent{}

type cContent struct {

}

func (a *cContent) GetContentById(c *gin.Context) {
	pid, _ := c.GetQuery("pid")
	service.Content.GetContentByPath(c, pid)
}



func (a *cContent) SaveContent(c *gin.Context) {
	content := service.ContentDto{}
	err := c.ShouldBind(&content)
	if err != nil {
		response.Json(c, 422, "代码保存失败", fmt.Sprintf("%v", err))
		return
	}
	service.Content.SaveContent(c, content)
}