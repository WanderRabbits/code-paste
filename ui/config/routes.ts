export default [
  { 
    path: '/',
    component: '../layouts/BasicLayout',
    routes: [
      { 
        name: '表单',
        path: '/', 
        exact: true,
        component: '@/pages/index'
      },
      {
        name: '404',
        path: '/404',
        exact: true,
        component: '@/pages/404'
      },
      {
        name: '内容',
        path: '/:pid',
        exact: true,
        component: '@/pages/content'
      },
      {
        component: './404',
      },
    ]
  },
  {
    component: './404',
  },
]