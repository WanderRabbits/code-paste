import { defineConfig } from 'umi';
import routes from './routes';
import defaultSettings from './defaultSettings';

const {REACT_APP_ENV} = process.env;

const IS_PROD = process.env.NODE_ENV !== 'development';

export default defineConfig({
  hash: false,
  antd: {},
  dva: {
    hmr: true,
  },
  nodeModulesTransform: {
    type: 'none',
  },
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    'primary-color': defaultSettings.primaryColor,
  },
  targets: {
    ie: 11,
  },
  routes,
  mfsu: {},
  dynamicImport: {
    loading: '@ant-design/pro-layout/es/PageLoading',
  },
  fastRefresh: {},
  proxy: {
    '/api': {
      'target': 'http://localhost:8080/',
      'changeOrigin': true,
      // 'pathRewrite': { '^/api' : '' },
    },
  },
  manifest: {
    basePath: '/',
  },
  esbuild: {},
  webpack5: {},
  exportStatic: {},
  //mock: false,
});
