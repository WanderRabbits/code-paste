export default {
	'GET /api/getContent': {
		code: 200,
		message: 'OK',
		data: {
			content: `#include <iostream>
using namespace std;
int main() {
	cout << "Hello, World!" << '\\n';
	return 0;
}`,
			type: 'cpp'
		}
	},
	'POST /api/generateContent': {
		code: 200,
		message: 'OK',
		data: {
			path: '2a135'
		}
	}
}