import { Card } from "antd";
import {marked} from "marked";
import hljs from "highlight.js";
import 'highlight.js/styles/github.css'
import styles from './index.less'
import CopyToClipboard from "react-copy-to-clipboard";


const typeOptions = {
  cpp: 'C++',
  c: 'C',
  java: 'Java',
  text: 'TXT',
  js: 'JavaScript', 
}

export const CodePreview: React.FC<{
  children?: React.ReactNode,
  type?: string,
  code?: string
}> = (props) => {
  const {code, type} = props;
  // 配置 marked
  const renderer = new marked.Renderer()
  marked.setOptions({
    renderer: renderer,  // 这个是必须填写的
    gfm: true,  // 启动类似Github样式的Markdown,
    pedantic: false,  // 只解析符合Markdown定义的，不修正Markdown的错误
    sanitize: false,  // 原始输出，忽略HTML标签
    breaks: false,  // 支持Github换行符，必须打开gfm选项
    smartLists: true,  // 优化列表输出
    smartypants: false,
    // 高亮显示规则 ，这里使用highlight.js来完成
    highlight: function (code:string) {
      return hljs.highlightAuto(code).value
    }
  })
  let html = marked(code ?? '')
  return (
    <Card 
      type="inner" 
      title={typeOptions[type ?? 'text']} 
      extra={	<CopyToClipboard
        text={code ?? ''}
      >
        <a>复制</a>
      </CopyToClipboard>}
      >
      <div className={styles.content}>
        <div
          id="content"
          className="article-detail"
          dangerouslySetInnerHTML={{
            __html: html,
          }}
        />
      </div>
    </Card>
  );
}


/**
 * 为代码块显示添加行号
 * @param {String} code MD的代码内容
 */
 function beforNumber(code: string) {
  if (!code.trim()) {
    return code;
  }
  const list = code.split('\n');
  const spanList = ['<span aria-hidden="true" line-row>'];
  list.forEach(() => {
    spanList.push('<span></span>');
  });
  spanList.push('</span>');
  list.push(spanList.join(''));
  return list.join('\n');
}
