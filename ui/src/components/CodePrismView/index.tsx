import { useEffect, useRef } from 'react';
import { Card } from 'antd';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import prism from '@/utils/prism';

export const typeOptions = {
  text: 'PlainText',
  js: 'JavaScript',
  cpp: 'C++',
  c: 'C',
  java: 'Java',
  csharp: 'C#',
  css: 'CSS',
  erlang: 'Erlang',
  golang: 'Golang',
  haskell: 'Haskell',
  html: 'HTML',
  json: 'JSON',
  kotlin: 'Kotlin',
  php: 'PHP',
  python: 'Python',
  rust: 'RUST',
  sql: 'SQL',
  ruby: 'Ruby',
  scala: 'Scala',
  typescript: 'Typescript',
};

type MarkdownViewProps = {
  code: string;
  type: string;
};

const CodeView: React.FC<MarkdownViewProps> = (props) => {
  const { type, code } = props;
  const divRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    setImmediate(() => {
      if (divRef.current) {
        prism.highlightAllUnder(divRef.current, false);
      }
    });
  });
  return (
    <div style={{ width: '100%' }} ref={divRef}>
      <pre>
        <code className={`language-${type}`}>{code}</code>
      </pre>
    </div>
  );
};

export const PreView: React.FC<MarkdownViewProps & { type: string }> = (
  props,
) => {
  const { type, code } = props;
  return (
    <Card
      type="inner"
      title={typeOptions[type]}
      extra={
        <CopyToClipboard text={code}>
          <a>复制</a>
        </CopyToClipboard>
      }
    >
      <div
        style={{
          width: '100%',
          // flexGrow: 1
        }}
        className="code-area line-numbers"
      >
        <CodeView code={code} type={type} />
      </div>
    </Card>
  );
};
