import React from 'react';
import type {
  BasicLayoutProps as ProLayoutProps,
} from '@ant-design/pro-layout';
import { Input, Layout } from 'antd';
const { Header, Content, Footer } = Layout;
export interface BasicLayoutProps extends ProLayoutProps {
  route: ProLayoutProps['route'] & {
    authority: string[];
  };
}
import {useHistory} from 'umi'
import './index.css'


const {Search} = Input;
const BasicLayout: React.FC<BasicLayoutProps> = (props) => {
	const {
		children,
	} = props;
	const history = useHistory();
	const onSearch = (value: string) => {
		history.push('/' + value);
	}

	return (
		<Layout>
			<Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
				<div className="logo">
						<Search
							addonBefore="code-paste/"
							placeholder="索引"
							allowClear
							onSearch={onSearch}
							style={{ width: 304, marginTop: 15 }}
							enterButton="前往"
						/>
				</div>
			</Header>
		  
		<Content className="site-layout" style={{ padding: '0 50px', marginTop: 64 }}>
			<br/>
      <div className="site-layout-background" style={{ padding: 24, minHeight: 380 }}>
			{children}
      </div>
    </Content>
    <Footer style={{ textAlign: 'center' }}>Copyright ©{(function () {
			const year = new Date().getFullYear();
			return year === 2022 ? '2022' : `2022 - ${year}`;
		})()} KAMIBAKO</Footer>
		</Layout>
	)
}

export default BasicLayout;