import { useParams } from 'umi'
import { useState, useEffect } from 'react'
import { getContent }from '@/services/api'
import { PageContainer } from '@ant-design/pro-layout'
import { PageLoading } from '@ant-design/pro-layout'
import { PreView } from '@/components/CodePrismView'
export default () => {
	const params = useParams<{pid: string}>();
	const {pid} = params;
	const [isGotContent, setIsGotContent] = useState<boolean>(false)
	const [content, useContent] = useState<string>('');
	const [type, useType] = useState<string>('text');

	useEffect(() => {
		getContent(pid).then(res => {
			//console.log(res);
			if (res.data) {
				useContent(res.data.content);
				useType(res.data.type);
				setIsGotContent(true)
			}
		})
	}, [])
	return (
		<PageContainer>
			{
				isGotContent ? 
				<PreView type={type} code={content}/> :
				<PageLoading/>
			}
		</PageContainer>
	)
}