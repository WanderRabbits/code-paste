import React from 'react';
import { useState } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import { Form, Button, Input, Select, Tooltip } from 'antd';
import { submitForm } from '@/services/api';
const { Option } = Select;
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 12 },
};
import CopyToClipboard from 'react-copy-to-clipboard';
const validateMessages = {
  required: '${label} is required!',
};

const handleChange = (value: string) => {
  //console.log(`selected ${value}`);
};

const options = [
  { k: 'text', v: 'PlainText' },
  { k: 'js', v: 'JavaScript' },
  { k: 'cpp', v: 'C++' },
  { k: 'c', v: 'C' },
  { k: 'java', v: 'Java' },
  { k: 'csharp', v: 'C#' },
  { k: 'css', v: 'CSS' },
  { k: 'erlang', v: 'Erlang' },
  { k: 'golang', v: 'Golang' },
  { k: 'haskell', v: 'Haskell' },
  { k: 'html', v: 'HTML' },
  { k: 'json', v: 'JSON' },
  { k: 'kotlin', v: 'Kotlin' },
  { k: 'php', v: 'PHP' },
  { k: 'python', v: 'Python' },
  { k: 'ruby', v: 'Ruby' },
  { k: 'scala', v: 'Scala' },
  { k: 'typescript', v: 'Typescript' },
  { k: 'rust', v: 'RUST' },
  { k: 'sql', v: 'SQL' },
];

export default (): React.ReactNode => {
  const [finished, useFinished] = useState<boolean>(false);
  const [contentIndex, useContentIndex] = useState<string>('');
  const onFinish = (values: any) => {
    //console.log(values);
    submitForm(values).then((res) => {
      if (res.message && res.message === 'OK') {
        useContentIndex(res.data.path);
        useFinished(true);
      }
    });
  };

  return (
    <PageContainer>
      {finished === false ? (
        <Form
          {...layout}
          name="messages"
          initialValues={{ type: 'text' }}
          onFinish={onFinish}
          validateMessages={validateMessages}
        >
          <Form.Item name="type" label="高亮">
            <Select style={{ width: 200 }} onChange={handleChange}>
              {options.map((item) => (
                <Option value={item.k} key={item.k}>
                  {item.v}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item name="content" label="内容">
            <Input.TextArea
              placeholder="输点什么吧"
              autoSize={{ minRows: 10, maxRows: 10 }}
            />
          </Form.Item>
          <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
            <Button type="primary" htmlType="submit">
              保存
            </Button>
          </Form.Item>
        </Form>
      ) : (
        <div style={{ margin: '0 auto 0 auto' }}>
          <h1>保存成功</h1>
          欲访问<strong>{contentIndex}</strong>所对应的文本
          <ul style={{ listStyleType: 'circle' }}>
            <li>
              在导航栏中输入<strong>索引</strong>
            </li>
            <li>
              在浏览器中访问
              <Tooltip placement="bottom" title="点击复制链接">
                <CopyToClipboard text={`${process.env.URL}${contentIndex}`}>
                  <a>
                    {process.env.URL}
                    {contentIndex}
                  </a>
                </CopyToClipboard>
              </Tooltip>
            </li>
          </ul>
          <Button type="primary" onClick={() => useFinished(false)}>
            返回主页
          </Button>
        </div>
      )}
    </PageContainer>
  );
};
