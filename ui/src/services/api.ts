import request from '@/utils/request'

export async function getContent(pid: string) {
	return request.get('/api/getContent', { 
		params: {
			pid : pid
		} 
	})
}

export async function submitForm(values: any) {
	return request.post('/api/generateContent', {
		params: values,
	})
}